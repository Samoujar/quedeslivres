<?php
namespace App\Controller\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BookRepository;
use App\Repository\OptionRepository;
use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\BookType;
use App\Entity\Book;

use Symfony\Component\Security\Core\Security;





class AdminBookController extends AbstractController 
{
    /**
     * @var BookRepository
     */
    private $repository;

    private $security;

    public function __construct(BookRepository $repository,ObjectManager $em, Security $security){
        $this->repository = $repository;
        $this->em = $em ;
        $this->security = $security;
    }
    /**
     * @Route("/error",name="error")
     */

    public function error(){
        return $this->render('pages/error404.html.twig',[
            "options"=>""
        ]
    );

    }

    /**
     * * @Route("/admin" ,name="admin.index")
     */

public function index(UserInterface $user,OptionRepository $optionRepository, BookRepository $bookRepository)
{
  

    if (!$this->security->isGranted('ROLE_ADMIN')) {
        $books = $bookRepository->findLatest();
        $message ;
        return $this->redirectToRoute('error', [
            //"message"=>$message,
            'books'=> $books,
            "options" => $optionRepository->findAll()
        ], 301);
    }
$books = $this->repository->findAll();
$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

return $this->render('admin/index.html.twig',[
            'books'=> $books,
            "options" => $optionRepository->findAll(),
            "user"=>$user
]);

}

/**
 * @Route("/admin",name="admin.tt")
 */
public function adminUser(UserInterface $user){
    $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

    $this->render('admin/users.html.twig');
}



}

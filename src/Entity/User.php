<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"username"}, message="Ce nom d'utilisateur est déja pris")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="6",minMessage="Votre Mots de passer doit avoir 6 caractéres au minimum")
     * @Assert\EqualTo(propertyPath="confirm_password",)
     */
    private $password;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @Assert\EqualTo(propertyPath="password", message="Vous n'avez pas tappé le même Mots de passer,veuillez réessayer !")
     */
 /*   private $confirm_password;

    public function getConfirmPassword(): ?string
    {
        return $this->confirm_password;
    }

    public function setConfirmPassword(string $confirm_password): self
    {
        $this->confirm_password = $confirm_password;

        return $this;
    }*/
   
    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId(int $id)
    {
         $this->id = $id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
   

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Book", mappedBy="user")
     */
    private $relation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Book", mappedBy="user")
     */
    private $books;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contact", mappedBy="user")
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="user")
     */
    private $comments;

    public function __construct()
    {
        $this->relation = new ArrayCollection();
        $this->books = new ArrayCollection();
        $this->book = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }


public function getSalt(){
    return null ;
}
public function eraseCredentials(){

}

public function serialize(){
 return $this->serialize([ 
        $this->id,
        $this->username,
        $this->passowrd
      ]);
        
}

public function unserialize(){
 list (
        $this->id,
        $this->username,
        $this->password
      ) = unserialize($serialized, ['allowed_classes'=> false]);
}

/**
 * @return Collection|Book[]
 */
public function getRelation(): Collection
{
    return $this->relation;
}

public function addRelation(Book $relation): self
{
    if (!$this->relation->contains($relation)) {
        $this->relation[] = $relation;
        $relation->setUser($this);
    }

    return $this;
}

public function removeRelation(Book $relation): self
{
    if ($this->relation->contains($relation)) {
        $this->relation->removeElement($relation);
        // set the owning side to null (unless already changed)
        if ($relation->getUser() === $this) {
            $relation->setUser(null);
        }
    }

    return $this;
}

/**
 * @return Collection|Book[]
 */
public function getBooks(): Collection
{
    return $this->books;
}

public function addBook(Book $book): self
{
    if (!$this->books->contains($book)) {
        $this->books[] = $book;
        $book->setUser($this);
    }

    return $this;
}

public function removeBook(Book $book): self
{
    if ($this->books->contains($book)) {
        $this->books->removeElement($book);
        // set the owning side to null (unless already changed)
        if ($book->getUser() === $this) {
            $book->setUser(null);
        }
    }

    return $this;
}

public function getEmail(): ?string
{
    return $this->email;
}

public function setEmail(?string $email): self
{
    $this->email = $email;

    return $this;
}

/**
 * @return Collection|Contact[]
 */
public function getContacts(): Collection
{
    return $this->contacts;
}

public function addContact(Contact $contact): self
{
    if (!$this->contacts->contains($contact)) {
        $this->contacts[] = $contact;
        $contact->setUser($this);
    }

    return $this;
}

public function removeContact(Contact $contact): self
{
    if ($this->contacts->contains($contact)) {
        $this->contacts->removeElement($contact);
        // set the owning side to null (unless already changed)
        if ($contact->getUser() === $this) {
            $contact->setUser(null);
        }
    }

    return $this;
}

/**
 * @return Collection|Comment[]
 */
public function getComments(): Collection
{
    return $this->comments;
}

public function addComment(Comment $comment): self
{
    if (!$this->comments->contains($comment)) {
        $this->comments[] = $comment;
        $comment->setUser($this);
    }

    return $this;
}

public function removeComment(Comment $comment): self
{
    if ($this->comments->contains($comment)) {
        $this->comments->removeElement($comment);
        // set the owning side to null (unless already changed)
        if ($comment->getUser() === $this) {
            $comment->setUser(null);
        }
    }

    return $this;
}


public function getRoles(): array
{
    $roles = $this->roles;
    // guarantee every user at least has ROLE_USER
    $roles[] = 'ROLE_USER';

    return array_unique($roles);
}


public function addRoles($role){
    $this->roles[] = $role;

    return $this;
    
}

}

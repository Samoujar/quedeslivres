<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use App\Repository\BookRepository;
use App\Repository\OptionRepository;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


use Twig\Environment;



Class HomeController extends AbstractController
{
    /**
     * @var Envirenment
     */

    private $twig;

    public function __construct($twig){
        $this->twig = $twig;
    }
    /**
     * @Route("/" ,name="home")
     * @return Response
     */

    public function index(BookRepository $repository,OptionRepository $optionRep): Response
    {
        $books = $repository->findLatest();
        return $this->render('pages/home.html.twig',[
            'books'=> $books,
            "options" => $optionRep->findAll(),
            "message"=>""

        ]);
    }
}
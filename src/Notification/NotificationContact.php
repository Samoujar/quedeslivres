<?php


namespace App\Notification;

use App\Entity\Contact;
use Twig\Environment;
use App\Entity\User;
use App\Entity\Book;

class NotificationContact {

    /**
 * @ \Swift_Mailer
 */

private $mailer;

/**
 * @var Environment
 */
private $renderer;

public function __construct(\Swift_Mailer $mailer, Environment $renderer){
    $this->mailer= $mailer;
    $this->renderer = $renderer;
}
public function notify(Contact $contact,User $receverUser,User $senderUser)
{
    $message = (new \Swift_Message('Book' . $contact->getBook()->getTitle(),$contact->getBook()->getUser()))
        ->setFrom($senderUser->getEmail())
        ->setTo($receverUser->getEmail())
        ->setBody($this->renderer->render('emails/contact.html.twig', [
            'contact' =>$contact,
            'sender'=>$senderUser,
            'recever'=>$receverUser
    ]),'text/html');
            $this->mailer->send($message);

    }
}


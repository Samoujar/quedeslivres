<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Option;
use App\Form\OptionType;
use App\Repository\OptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Book ;

/**
 * @Route("/option")
 */
class OptionController extends AbstractController
{
    /**
     * @Route("/", name="option.index", methods={"GET"})
     */
    public function index(OptionRepository $optionRepository): Response
    {
        return $this->render('option/index.html.twig', [
            'options' => $optionRepository->findAll(),
        ]);
    }
  
    /**
     * @Route("/new", name="option.new", methods={"GET","POST"})
     */
    public function new(OptionRepository $optionRepository,Request $request): Response
    {
        $option = new Option();
        $form = $this->createForm(OptionType::class, $option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($option);
            $entityManager->flush();

            return $this->redirectToRoute('option.index');
        }

        return $this->render('option/new.html.twig', [
            'option' => $option,
            'form' => $form->createView(),
            "options" => $optionRepository->findAll()
        ]);
    }
    /**
     * @Route("/{id}", name="option.booksByOptionId", methods={"GET"})
     */
    public function index2(OptionRepository $optionRepository, int $id): Response
    {
        $option = $optionRepository->findById($id)[0];
        return $this->render('option/booksByOptionId.html.twig', [
            'books' =>$option->getBooks(),
            'categorie'=>$option->getName(),
            "options" => $optionRepository->findAll()
        ]);
    }

    /**
     * @Route("/{id}", name="option.show", methods={"GET"})
     */
    public function show(Option $option): Response
    {
        return $this->render('option/show.html.twig', [
            'option' => $option,
        ]);
    }

    /**
     * @Route("/{id}/", name="option.edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Option $option): Response
    {
        $form = $this->createForm(OptionType::class, $option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('option.edite',['id'=> $option ->getId()]);
        }

        return $this->render('option/edit.html.twig', [
            'option' => $option,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="option.delete", methods={"DELETE"})
     */
    public function delete(Request $request, Option $option): Response
    {
        if ($this->isCsrfTokenValid('delete'.$option->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($option);
            $entityManager->flush();
        }

        return $this->redirectToRoute('option.index');
    }
}

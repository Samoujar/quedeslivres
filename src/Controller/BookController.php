<?php

namespace App\Controller;


use App\Entity\Book;
use App\Form\BookType;

use App\Entity\User;
use App\Entity\Option;

use App\Entity\Comment;
use App\Entity\Contact;

use App\Form\OptionType;
use App\Form\CommentType;
use App\Form\ContactType;
use App\Repository\BookRepository;
use App\Repository\UserRepository;
use App\Repository\OptionRepository;
use App\Notification\NotificationContact;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Core\User\UserInterface;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;





class BookController extends AbstractController{


    /**
     * @var BookRepository
     */

     public function __construct(BookRepository $repository,ObjectManager $em)
     {
         $this->repository = $repository ;
         $this->em = $em;
     }

    /**
     * @Route("/biens" , name="book.index")
     * @return Response
     */

    public function index(PaginatorInterface $paginator,Request $request ,OptionRepository $optionRep):Response
    {
               $book = $paginator->paginate(
                $this->repository->findAllVisibleQuery(),
                $request->query->getInt('page',1),5
            );


        return $this->render('book/index.html.twig',[
            "current_menu" =>"book",
            "books" => $book,
            "options" => $optionRep->findAll()
        ]);
    }
    /**
     * @Route("/biens/{slug}-{id}", name="book.show",requirements ={"slug" : "[a-z0-9\-]*"})
     * @return Response
     */

    public function show(UserRepository $ur,NotificationContact $notification ,Book $book, string $slug,OptionRepository $optionRep,AuthenticationUtils $authenticationUtils,Request $request): Response
    {
       
    if($book->getSlug() !== $slug){
            $this->redirectToRoute('book.show',[
                'id' => $book->getId(),
                    'slug' => $book->getSlug()
            ],301);
}

    $contact= new Contact();
   
   
    $contact->setBook($book);
    $uid = $book->getUser()->getId();

    $usero= $ur->findById($uid)[0];
   // dd($user->getContacts());
    $form = $this->createForm(ContactType::class, $contact);
    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()){
       
       // dd($contact);
        
      $contact->setUser($user);
        $notification->notify($contact,$usero,$user);
        $userId= $book->getUser($book);
        $entityManager = $this->getDoctrine()->getManager();
        
        $entityManager->persist($contact);
        $entityManager->flush();
        $this->addFlash('success','votre commentaire à été bien ajoutter !');
       /* return $this->redirectToRoute('book.show',[
            'id' => $book->getId(),
                'slug' => $book->getSlug()
        ]);
        */
    }
        /*$book = $this->repository->find($id);*/
            return $this->render('book/show.html.twig',[
                "book"=> $book,
                
                "current_menu"=> "books",
                "options" => $optionRep->findAll(),
                "form"=>$form->createView()
            ]);
    }
    /**
     * @Route("/comments/{id}",name="comment.show")
     */
    public function comments(UserInterface $user,OptionRepository $optionRep,Request $request,Book $book,BookRepository $repository, int $id):Response
    {  
       //dd($comments);
       $comment = new Comment();
       $form = $this->createForm(CommentType::class,$comment);
       $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
          $comment->setBook($book);
          $comment->setUser($user);
           $user= $comment->getUser()->getId();

         // dd($comment);
             $entityManager = $this->getDoctrine()->getManager();
                
             $entityManager->persist($comment);
          
             $entityManager->flush();
             $this->addFlash('success','votre email à été bien envoyé !');
            return $this->redirectToRoute('comment.show',[
                
               // "comment"=>$comments->getComments(),
                 'id' => $book->getId(),
                     'slug' => $book->getSlug()
             ]);
             
         }
     


         $book->getComments()->initialize();
         $comments = $book->getComments()->getSnapshot();
         //dd($comments);


        return $this->render('pages/comment.html.twig',[
            "comments"=>$comments,
            "user"=>$user,
            "book" => $book,
            "options" => $optionRep->findAll(),
            "form"=>$form->createView()
            
            
        ]);
    
    }
    /**
     * @Route("/bookByUser",name="bookByUser")
     */

    public function bookByUser(UserInterface $user,UserRepository  $userRepository,BookRepository $bookRepository): Response
    {
       // $loggedUser= $userRepository->findById($user->getId());
       $user->getBooks()->initialize();
        $books =$user->getBooks()->getSnapshot();

     
    // dd( $books);
       
       
        return $this->render('pages/bookByUser.html.twig',[

           "books"=>$books,
            "user"=>$user,
            "options"=>[]
        ]);
    }
    
/**
 * @Route("/user/create" , name="admin.new")
 */

public function new(Request $request,UserInterface $user)
{
   // dd($user);
    $book = new Book();
    $form = $this->createForm(BookType::class,$book);
    $form->handleRequest($request);
    if($form->isSubmitted() && $form->isValid()){
        $book->setUser($user);
        $this->em->persist($book);
        $this->em->flush();
        $this->addFlash('success','Votre article a été ajoutter avec succes !');
         return $this->redirectToRoute('bookByUser');
    }
    return $this->render('admin/new.html.twig',[
        "book"=> $book,
        "form" => $form->createView(),
        'options'=>[]

    ]);
}


/**
 * @Route("/user/{id}", name="admin.edit")
 */

public function edit(Book $book, Request $request)
{

    $form = $this->createForm(BookType::class, $book);
    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()){
        $this->em->flush();
        $this->addFlash('success','Votre article a été modifié avec succes !');
        return $this->redirectToRoute('bookByUser');
    }
return $this->render('admin/edit.html.twig',[
   "book"=> $book,
   "form" => $form->createView(),
   'options'=>[]

]);
}
/**
* @Route("/user/book/{id}", name="admin.delete", methods="DELETE")
*/
public function delete(Book $book,Request $request)
{
    if($this->isCsrfTokenValid('delete' . $book->getId(),$request->get('_token'))){
        $this->em->remove($book);
       $this->em->flush();
       $this->addFlash('success','Votre article a été supprimé avec succes !');
    }
    
    return $this->redirectToRoute('bookByUser');
}
}